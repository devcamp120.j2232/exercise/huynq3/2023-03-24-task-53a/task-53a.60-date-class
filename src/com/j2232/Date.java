package com.j2232;
import java.text.SimpleDateFormat;
import java.util.Date.*;


public class Date {
    private int day;
    private int month;
    private int year;
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setDate(int day, int month, int year){
      
        if (day<1||day>31||month<1||month>12||year<1900||year>9999){
            System.out.println("nhập ngày sai");
            
        }
        else {
           this.day=day;
           this.month=month;
           this.year=year;
                
            }
           
        }


    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormated = formatter.format(new java.util.Date(this.year - 1900, this.month - 1, this.day));
        
        System.out.println("Date [day=" + day + ", month=" + month + ", year=" + year + "]");
        return dateFormated;
    }
    
}
